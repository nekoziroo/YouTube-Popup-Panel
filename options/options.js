// i18n
const labelElement = document.getElementsByTagName('label');
labelElement[0].textContent = browser.i18n.getMessage('width');
labelElement[1].textContent = browser.i18n.getMessage('height');
labelElement[2].textContent = browser.i18n.getMessage('autoPlay');
labelElement[3].textContent = browser.i18n.getMessage('currentTime');

const inputWidth = document.getElementById('width');
const inputHeight = document.getElementById('height');
const inputAutoplay = document.getElementById('autoplay');
const inputcurrentTime = document.getElementById('currentTime');

function restoreWidth() {
  browser.storage.local.get('width', res => inputWidth.value = res.width || 960);
}
function restoreHeight() {
  browser.storage.local.get('height', res => inputHeight.value = res.height || 540);
}
function restoreAutoplay() {
  browser.storage.local.get('autoplay', res => inputAutoplay.checked = res.autoplay === undefined ? true : res.autoplay);
}
function restoreCurrentTime() {
  browser.storage.local.get('currentTime', res => inputcurrentTime.checked = res.autoplay === undefined ? true : res.autoplay);
}

function saveWidth() {
  const width = parseInt(inputWidth.value, 10);
  browser.storage.local.set({
    width: width,
  });
}
function saveHeight() {
  const height = parseInt(inputHeight.value, 10);
  browser.storage.local.set({
    height: height,
  });
}
function saveAutoplay() {
  browser.storage.local.set({
    autoplay: inputAutoplay.checked,
  });
}
function saveCurrentTime() {
  browser.storage.local.set({
    currentTime: inputcurrentTime.checked,
  });
}

document.addEventListener('DOMContentLoaded', restoreWidth);
document.addEventListener('DOMContentLoaded', restoreHeight);
document.addEventListener('DOMContentLoaded', restoreAutoplay);
document.addEventListener('DOMContentLoaded', restoreCurrentTime);
inputWidth.addEventListener('change', saveWidth);
inputHeight.addEventListener('change', saveHeight);
inputAutoplay.addEventListener('click', saveAutoplay);
inputcurrentTime.addEventListener('click', saveCurrentTime);

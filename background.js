const YouTube = {};
YouTube.func = {};
YouTube.Class = class {
  constructor(url) {
    const match = url.match(/^https:\/\/www\.youtube\.com\/watch\?v=(.*?)(?:&|$)(?:index=[\d]+)?&?(list=[^&#]+)?/);
    this.url = null;
    if (match[2]) {
      this.listId = match[2];
    }
    this.videoId = match[1];
  }
  getURL() {
    if (this.listId && YouTube.storage.autoplay === undefined ? true : YouTube.storage.autoplay) {
      return `https://www.youtube.com/embed/${this.videoId}?${this.listId}&autoplay=1`;
    } else if (this.listId) {
      return `https://www.youtube.com/embed/${this.videoId}?${this.listId}`;
    } else if (YouTube.storage.autoplay === undefined ? true : YouTube.storage.autoplay) {
      return `https://www.youtube.com/embed/${this.videoId}?autoplay=1`;
    } else {
      return `https://www.youtube.com/embed/${this.videoId}`;
    }
  }
};

YouTube.func.onMessage = function(message) {
  YouTube.currentTime = message.currentTime;
};
browser.runtime.onMessage.addListener(YouTube.func.onMessage);

// listen to tab switching
browser.tabs.onActivated.addListener(activeInfo => YouTube.func.updateActiveTab(activeInfo.tabId));
// listen to tab URL changes
browser.tabs.onUpdated.addListener((tabId, changeInfo) => {
  if (changeInfo.status === 'complete') {
    YouTube.func.updateActiveTab(tabId);
  }
});
YouTube.func.updateActiveTab = async function(tabs) {
  const tabInfo = await browser.tabs.get(tabs);
  if (/^https:\/\/www\.youtube\.com\/watch\?.*/.test(tabInfo.url)) {
    browser.pageAction.show(tabs);
  }
};
browser.pageAction.onClicked.addListener(tab => YouTube.func.onClickPageAction(tab));

YouTube.func.onClickPageAction = async function(tab) {
  YouTube.storage = await browser.storage.local.get();
  if (YouTube.storage.currentTime === undefined ? true : YouTube.storage.currentTime) {
    browser.tabs.onUpdated.addListener(YouTube.func.currentTime);
    await browser.tabs.executeScript(tab.id, {
      code: '{const vid = document.getElementsByClassName("video-stream")[0];const currentTime = vid.currentTime;' +
            'vid.pause();browser.runtime.sendMessage({currentTime: currentTime});};',
    });
  } else {
    await browser.tabs.executeScript(tab.id, {
      code: '{const vid = document.getElementsByClassName("video-stream")[0];vid.pause();};',
    });
  }
  const [width, height] = [YouTube.storage.width || 960, YouTube.storage.height || 540];
  const a = new YouTube.Class(tab.url);
  browser.windows.create({
    url   : a.getURL(),
    type  : 'panel',
    width : width,
    height: height,
  });
};

YouTube.func.currentTime = async function(tabId, changeInfo) {
  if (changeInfo.status === 'complete') {
    await browser.tabs.executeScript(window.id, {
      code: `{const vid = document.getElementsByClassName('video-stream')[0];vid.currentTime = ${YouTube.currentTime};};`,
    });
    browser.tabs.onUpdated.removeListener(YouTube.func.currentTime);
    delete YouTube.storage;
    delete YouTube.currentTime;
  }
};
